# Anibook Frontend

## Frontend para a aplicação [Anibook](https://github.com/Bruce2107/anibook-backend)

### Para iniciar
```sh
$ yarn start
```

### Testes
```sh
$ yarn test
```

#### _Dúvidas_
Para qualquer dúvida entre em contato via [Twitter](https://twitter.com/Bruce2107).

# License
[MIT](https://github.com/Bruce2107/anibook-frontend/blob/master/LICENSE)
