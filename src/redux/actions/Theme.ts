import { TOGGLE_THEME } from '../../constants';

export function ToggleTheme() {
  return {
    type: TOGGLE_THEME,
  };
}

export default ToggleTheme;
