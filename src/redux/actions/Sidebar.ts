import { TOGGLE_SIDEBAR } from '../../constants';

export function ToggleSidebar() {
  return {
    type: TOGGLE_SIDEBAR,
  };
}

export default ToggleSidebar;
