import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  title: 'light',
  colors: {
    primary: '#ffea00',
    secondary: '#444444',
    background: '#d8e0e9',
    backgroundCard: '#a9a9a9',
    text: '#333',
    footer: 'rgba(255, 206, 206, 0.5)',
    black: '#000',
    upColor: '#ffd500',
    downColor: '#ff1493',
    upColorInverted: '#00d2d3',
    downColorInverted: '#2e86de',
  },

};

export default theme;
